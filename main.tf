resource "openstack_networking_secgroup_v2" "this" {
  name        = var.secgroup_name
  description = var.secgroup_description
  region      = var.region
}

resource "openstack_networking_secgroup_rule_v2" "this" {
  count             = length(var.rules)
  direction         = var.rules[count.index].direction
  ethertype         = var.rules[count.index].ethertype
  protocol          = var.rules[count.index].protocol
  port_range_min    = var.rules[count.index].port_range_min
  port_range_max    = var.rules[count.index].port_range_max
  remote_ip_prefix  = var.rules[count.index].remote_ip_prefix
  security_group_id = openstack_networking_secgroup_v2.this.id
  region            = var.region
}

resource "openstack_networking_port_secgroup_associate_v2" "this" {
  port_id = var.port_id
  security_group_ids = [
    openstack_networking_secgroup_v2.this.id,
  ]
  region = var.region
}
