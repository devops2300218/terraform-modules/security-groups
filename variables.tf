variable "secgroup_name" {
  description = "Name of the security group"
  type        = string
}

variable "secgroup_description" {
  description = "Description of the security group"
  type        = string
  default     = "Managed by Terraform"
}

variable "rules" {
  description = "List of rules to apply to the security group"
  type = list(object({
    direction        = string
    ethertype        = string
    protocol         = string
    port_range_min   = number
    port_range_max   = number
    remote_ip_prefix = string
  }))
}
variable "port_id" {
  description = "The ID of the port to associate with this security group"
  type        = string
}
variable "region" {
  description = "The OVH Region"
  type        = string
}
